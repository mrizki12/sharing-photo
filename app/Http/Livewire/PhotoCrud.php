<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Photo;

class PhotoCrud extends Component
{

    public $photos, $name, $caption, $tags;
    public $isModalOpen = 0;

    public function render()
    {
        $this->photos = Photo::all();
        return view('livewire.photo.index');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }

    public function closeModal()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->caption = '';
        $this->tags = '';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|image',
            'caption' => 'required|string|max:255',
            'tags' => 'required|string|max:255'
        ]);

        $file = $request->file('name');

        /** Generate random unique_name for file */
        $fileName = time().md5(time()).'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/photo', $fileName);

        Photo::updateOrCreate(['name' => $this->name], [
            'name' => $fileName,
            'caption' => $this->caption,
            'tags' => $this->tags
        ]);

        session()->flash('message', $this->id ? 'Data berhasil diperbaharui.' : 'Data berhasil ditambahkan.');

        $this->closeModal();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $photo = Photo::findOrFail($id);
        $this->name = $photo->name;
        $this->caption = $photo->caption;
        $this->tags = $photo->tags;

        $this->openModal();
    }

    public function delete($id)
    {
        Photo::find($id)->delete();
        session()->flash('message', 'Data berhasil dihapus.');
    }
}
