# Dokumentasi Project

Program ini adalah test case untuk backend developer di vhiweb, dibuat dengan framework laravel dan menggunakan beberapa package dibawah ini :

  - Jetstream
  - Livewire
  - Sanctum

Untuk Authentikasi API memakai bearer token.

# Instalasi
Silahkan mengikuti beberapa arahan berikut dengan ketik di terminal anda :

1. `git clone https://mrizki12@bitbucket.org/mrizki12/sharing-photo.git`
2. `composer install`
3. `cp .env.example .env`
4. `php artisan key:generate`
5. `php artisan serve`

# API Endpoint
Setalah semua langkah tadi selesai project sudah bisa dijalankan, dan berikut beberapa endpoint yang ada:

1. AUTH

- POST `/register`, untuk membuat akun

    Key yang dibutuhkan di Body : name, email, password

- POST `/login`, untuk login dengan akun yang sudah dibuat

    Key yang dibutuhkan di Body : email, password

2. Photo

- GET `api/photos`, untuk mendapatkan semua data foto yang ada

- GET `api/photos/:id`, untuk mendapatkan data foto berdasarkan id

- POST `api/photos`, untuk membuat sebuah foto dari user yang sudah login

    Key yang dibutuhkan di Body : name, caption, tags

- PUT `api/photos/:id`, untuk memperbarui data tertentu (caption, tags) pada foto yang dimiliki oleh user

    Key yang dibutuhkan di Params : caption, tags

- POST `api/photos/:id/like`, untuk menyukai sebuah foto

- POST `api/photos/:id/unlike`, untuk menghapus penyukaan sebuah foto

Untuk semua request api/photos memerlukan token yang didapat ketika anda login, masukan token tersebut di bagian authorization dan pilih type bearer token.
