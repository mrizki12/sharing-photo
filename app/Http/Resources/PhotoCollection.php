<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PhotoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data'=>[
                'photo'=>$this->collection->map(function($data){
                    return [
                        'id'=>$data->id,
                        'name'=>$data->name,
                        'user_id'=>$data->user_id,
                        'caption'=>$data->caption,
                        'tags'=>$data->tags,
                        'date'=> date('d-m-Y', strtotime($data->created_at))
                    ];
                })
            ]
        ];
    }
}
