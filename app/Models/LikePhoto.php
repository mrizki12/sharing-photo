<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LikePhoto extends Model
{
    use HasFactory;

    protected $table = 'like_photos';
    protected $fillable = ['photo_id', 'user_id', 'status'];
}
