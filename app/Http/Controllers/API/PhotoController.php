<?php

namespace App\Http\Controllers\API;
use Validator;
use App\Models\Photo;
use App\Models\LikePhoto;
use Auth;
use App\Http\Resources\PhotoCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\PhotoResource;

class PhotoController extends Controller
{
    public function index()
    {
        $data = Photo::toBase('id, name, user_id, caption, tags, created_at as date')->get();
        if(is_null($data)){
            return response()->json('Data not found.', 404);
        }

        return new PhotoCollection($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required|image',
            'caption'=>'required|string',
            'tags'=>'required|string'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $file = $request->file('name');

        /** Generate random unique_name for file */
        $fileName = time().md5(time()).'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/photo', $fileName);

        $photo = Photo::create([
            'name'=>$fileName,
            'user_id'=>auth()->user()->id,
            'caption'=>$request->caption,
            'tags'=>$request->tags
        ]);

        return response()->json(['success'=>true, 'messages'=>'Photo successfully added!']);
    }

    public function show($id)
    {
        $user_id = auth()->user()->id;

        $photo = Photo::leftJoin('like_photos', 'like_photos.photo_id', 'photos.id')->leftJoin('users', 'users.id', 'photos.user_id')
        ->selectRaw('photos.id, photos.name, coalesce(count(like_photos.user_id),0) AS total_like, users.name as from_user, photos.caption, photos.tags')
        ->selectRaw("CASE WHEN (SELECT COUNT(user_id) FROM like_photos WHERE user_id='$user_id' and photo_id='$id') = 0 THEN 'not liked' ELSE 'liked' END AS status")
        ->where(['photos.id'=>$id])->groupBy('photos.id')->firstOrFail();

        if(is_null($photo)){
            return response()->json('Data not found.', 404);
        }

        return response()->json([new PhotoResource($photo)]);
    }

    public function update(Request $request, Photo $photo)
    {
        $validator = Validator::make($request->all(),[
            'caption'=>'required|string',
            'tags'=>'required|string'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $photo->caption = $request->caption;
        $photo->tags = $request->tags;
        $photo->save();

        return response()->json(['success'=>true, 'messages'=>'Photo successfully updated!']);
    }

    public function destroy($id)
    {
        $photo = Photo::find($id);

        if(is_null($photo)){
            return response()->json('Data not found.', 404);
        }

        $likePhoto = LikePhoto::where('photo_id', $id)->delete();
        $photo->delete();

        return response()->json(['success'=>true, 'messages'=>'Photo successfully deleted!']);
    }

    public function like($photo_id)
    {
        $photo = Photo::find($photo_id);
        if(is_null($photo)){
            return response()->json('Data not found.', 404);
        }

        $like = LikePhoto::create(['photo_id'=>$photo_id, 'user_id'=>auth()->user()->id, 'status'=>'like']);

        return response()->json(['success'=>true, 'messages'=>'Photo successfully liked!']);
    }

    public function unlike($photo_id)
    {

        $photo = Photo::find($photo_id);
        if(is_null($photo)){
            return response()->json('Data not found.', 404);
        }

        $unlike = LikePhoto::where(['photo_id'=>$photo_id, 'status'=>'like'])->delete();

        return response()->json(['success'=>true, 'messages'=>'Unlike photo successfully!']);
    }
}
