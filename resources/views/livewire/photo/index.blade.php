<x-slot name="header">
    <h2 class="text-center">Data Photo</h2>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            @if (session()->has('message'))
            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                role="alert">
                <div class="flex">
                    <div>
                        <p class="text-sm">{{ session('message') }}</p>
                    </div>
                </div>
            </div>
            @endif
            <button wire:click="create()" class="bg-gray-800 text-white font-bold py-2 px-4 rounded my-3">Add Photo</button><br>
            @if($isModalOpen)
            @include('livewire.photo.create')
            @endif
            <table class="table-responsive-sm w-full">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="px-4 py-2 w-20 text-center">Name</th>
                        <th class="px-4 py-2 text-center">Caption</th>
                        <th class="px-4 py-2 text-center">Tags</th>
                        <th class="px-4 py-2 text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($photos as $photo)
                    <tr>
                        <td class="border px-4 py-2 text-center">{{ $photo->name }}</td>
                        <td class="border px-4 py-2 text-center">{{ $photo->caption }}</td>
                        <td class="border px-4 py-2 text-center">{{ $photo->tags }}</td>
                        <td class="border px-4 py-2 text-center">
                            <button wire:click="edit({{ $photo->id }})"
                                class="bg-indigo-600 text-white font-bold py-2 px-4 rounded">Edit</button>
                            <button
                                class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" data-toggle="modal" data-target="#deleteModal">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- Modal -->
            <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Confirm</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true close-btn">×</span>
                            </button>
                        </div>
                       <div class="modal-body">
                            <p>Are you sure?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                            <button type="button" wire:click="delete({{ $photo->id }})" class="btn btn-danger close-modal" data-dismiss="modal">Yes, Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
